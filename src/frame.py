#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Assemble Frames
"""
import collections
import itertools
import struct
import os

from .utils import _logger
from .exceptions import WebSocketBadStatusException, WebSocketFrameException

try:
    from .speedups import apply_mask
except ImportError:
    def apply_mask(data, mask_key):
        if not data:
            data = b""
        data_bytes = bytes(b ^ m for b, m in zip(data, itertools.cycle(mask_key)))
        return mask_key + data_bytes

"""
https://tools.ietf.org/html/rfc6455#page-25
"""

OPCODE_CONT = 0x0
OPCODE_TEXT = 0x1  # non-control
OPCODE_BINARY = 0x2  # non-control
OPCODE_CLOSE = 0x8  # control
OPCODE_PING = 0x9  # control
OPCODE_PONG = 0xa  # control

OPCODES = (OPCODE_CONT, OPCODE_TEXT, OPCODE_BINARY, OPCODE_CLOSE,
           OPCODE_PING, OPCODE_PONG)

OPCODE_MAP = {
    OPCODE_CONT: "cont",
    OPCODE_TEXT: "text",
    OPCODE_BINARY: "binary",
    OPCODE_CLOSE: "close",
    OPCODE_PING: "ping",
    OPCODE_PONG: "pong"
}

# closing frame status codes.
STATUS_NORMAL = 1000
STATUS_GOING_AWAY = 1001
STATUS_PROTOCOL_ERROR = 1002
STATUS_UNSUPPORTED_DATA_TYPE = 1003
STATUS_STATUS_NOT_AVAILABLE = 1005
STATUS_ABNORMAL_CLOSED = 1006
STATUS_INVALID_PAYLOAD = 1007
STATUS_POLICY_VIOLATION = 1008
STATUS_MESSAGE_TOO_BIG = 1009
STATUS_INVALID_EXTENSION = 1010
STATUS_UNEXPECTED_CONDITION = 1011
STATUS_BAD_GATEWAY = 1014
STATUS_TLS_HANDSHAKE_ERROR = 1015

VALID_CLOSE_STATUS = (
    STATUS_NORMAL,
    STATUS_GOING_AWAY,
    STATUS_PROTOCOL_ERROR,
    STATUS_UNSUPPORTED_DATA_TYPE,
    STATUS_INVALID_PAYLOAD,
    STATUS_POLICY_VIOLATION,
    STATUS_MESSAGE_TOO_BIG,
    STATUS_INVALID_EXTENSION,
    STATUS_UNEXPECTED_CONDITION,
    STATUS_BAD_GATEWAY,
)

LENGTH_7 = 0x7e
LENGTH_16 = 1 << 16
LENGTH_64 = 1 << 63

Frame = collections.namedtuple(
    'Frame',
    ['fin', 'rsv1', 'rsv2', 'rsv3', 'opcode', 'mask', 'data']
)


def reverse_mask(data, mask_key):
    return apply_mask(data, mask_key)


class ABNF(Frame):

    def __new__(cls, fin=1, rsv1=0, rsv2=0, rsv3=0, opcode=OPCODE_TEXT, mask=1, data=""):
        return Frame.__new__(cls, fin, rsv1, rsv2, rsv3, opcode, mask, data)

    def get_mask_key(self):
        if self.mask:
            return os.urandom(4)
        else:
            return b''

    @staticmethod
    def create(opcode=OPCODE_TEXT, data="", mask=0, fin=1):
        if isinstance(data, bytes):
            pass
        elif isinstance(data, str):
            data = data.encode('utf-8')
        elif isinstance(data, int):
            data = str(data).encode("utf-8")
        return ABNF(fin, 0, 0, 0, opcode, mask, data)# mask =1

    def encode_frame(self, cont=False):
        """
        encode object as byte string
        """
        if not cont:
            length = len(self.data)
            mask_key = self.get_mask_key()
            frame_header = self.get_frame_header(self.fin, self.opcode, self.mask, length)
            frame_data = self.get_frame_data(self.data, mask_key)
            frame = frame_header + frame_data
            return frame
        else:
            if isinstance(self.data, list):
                result = []
                length = len(self.data)
                mask_key = self.get_mask_key()
                for i in range(length):
                    if (i > 0) & (i < length - 1):
                        length_data = len(self.data[i])
                        header = self.get_frame_header(0, OPCODE_CONT, self.mask, length_data)
                        data = self.get_frame_data(self.data[i], mask_key)
                        result.append(header + data)
                    elif i == 0:
                        length_data = len(self.data[i])
                        header = self.get_frame_header(0, self.opcode, self.mask, length_data)
                        data = self.get_frame_data(self.data[i], mask_key)
                        result.append(header + data)
                    else:
                        length_data = len(self.data[i])
                        header = self.get_frame_header(1, OPCODE_CONT, self.mask, length_data)
                        data = self.get_frame_data(self.data[i], mask_key)
                        result.append(header + data)
            return result

    def get_frame_header(self, fin, opcode, mask, length):
        # return header of frame
        if length > LENGTH_64:
            return "Too long"

        frame_header = (
            fin << 7
            | self.rsv1 << 6
            | self.rsv2 << 5
            | self.rsv3 << 4
            | opcode
        )
        mask = 0b10000000 if mask else 0
        # = 125\126\127
        if length < LENGTH_7:
            frame_header = struct.pack("!BB", frame_header, mask | length)
        elif length < LENGTH_16:
            frame_header = struct.pack("!BBH", frame_header, mask | 126, length)
        elif length < LENGTH_64:
            frame_header = struct.pack("!BBQ", frame_header, mask | 127, length)
        return frame_header

    def get_frame_data(self, data, mask_key):
        if isinstance(data, bytes):
            pass
        else:
            data = data.encode('utf-8')
        if mask_key:
            mask_key = self.get_mask_key()
            masked_data = apply_mask(data, mask_key)
            return masked_data
        else:
            return data

    def close_frame(self, reason):
        # TODO send and response close
        pass

    def __str__(self):
        return f'FIN: {self.fin}, OPCODE: {OPCODE_MAP[self.opcode]}, DATA: {self.data}'

    __repr__ = __str__


class ResponseFrame(object):

    def __init__(self, conn):
        self.conn = conn
        self.length = 0
        self.mask_key = b''
        self.data = ""

    async def recv_frame(self):
        header = await self.recv_header()
        opcode = header[4]
        if opcode != OPCODE_CONT:
            mask_bit = header[5]
            length_bit = header[6]
            await self.recv_length(length_bit)
            if mask_bit:
                await self.recv_mask()
            data = await self.recv_data()
            return data.decode(), opcode
        else: # cont
            result = []
            while header[0]:
                mask_bit = header[5]
                length_bit = header[6]
                await self.recv_length(length_bit)
                if mask_bit:
                    await self.recv_mask()
                data = await self.recv_data()
                result.append(data.decode())
                header = await self.recv_header()
                _logger.debug(header)
            return result, opcode

    async def recv_header(self):
        # header check
        data = await self.conn.recv(2)
        header = struct.unpack('!BB', data)
        head1 = header[0]
        fin = (head1 & 0b10000000) >> 7
        rsv1 = (head1 & 0b01000000) >> 6
        rsv2 = (head1 & 0b00100000) >> 5
        rsv3 = (head1 & 0b00010000) >> 4
        opcode = head1 & 0b00001111

        if rsv1 or rsv2 or rsv3:
            raise WebSocketFrameException("rsv is not implemented.")

        if opcode not in OPCODES:
            raise WebSocketFrameException("Invalid opcode:", opcode)

        head2 = header[1]
        mask = head2 >> 7 & 1
        length = head2 & 0x7f

        return (fin, rsv1, rsv2, rsv3, opcode, mask, length)

    async def recv_length(self, length):
        # length
        if length <= 125:
            self.length = length
        elif length == 126:
            data = await self.conn.recv(2)
            self.length = struct.unpack("!H", data)[0]
        elif length == 127:
            data = await self.conn.recv(8)
            self.length = struct.unpack("!Q", data)[0]
        else:
            raise WebSocketFrameException("Frame length is too long.")

    async def recv_mask(self):
        self.mask_key = await self.conn.recv(4)

    async def recv_data(self):
        data = await self.conn.recv(self.length)
        if self.mask_key:
            data = reverse_mask(data, self.mask_key)
        else:
            pass
        self.data = data
        self.check_data()
        return data

    @staticmethod
    def check_status_code(code):
        if code in VALID_CLOSE_STATUS or (3000 <= code <= 5000):
            pass
        else:
            raise WebSocketBadStatusException("Close status code is not valid",code)

    def check_data(self):
        if isinstance(self.data, str):
            return self.data.encode('utf-8')
        elif isinstance(self.data, bytes):
            return self.data
        else:
            raise TypeError("data must be bytes or str")
