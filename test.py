#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import unittest
from unittest import mock
import asyncio

from src import utils
from src.connect import Connect
from src import frame
from src.handshake import Handshake
from src.exceptions import *


def asyncmock(*args, **kwargs):
    m = mock.MagicMock(*args, **kwargs)

    async def mock_coro(*args, **kwargs):
        return m(*args, **kwargs)

    mock_coro.mock = m
    return mock_coro


def _run(coro):
    return asyncio.get_event_loop().run_until_complete(coro)


class WS_Client(unittest.TestCase):

    def setUp(self):
        print('setUp...')

    def tearDown(self):
        print('tearDown...')

    def testParseUrl(self):
        r = utils.ws_urlparse("ws://www.example.com/r")
        self.assertEqual(r["scheme"], 'ws')
        self.assertEqual(r["host"], "www.example.com")
        self.assertEqual(r["port"], 80)
        self.assertEqual(r["resource"], '/r')
        self.assertEqual(r["query"], '')

        r = utils.ws_urlparse("wss://www.example.com/r")
        self.assertEqual(r["scheme"], 'wss')
        self.assertEqual(r["host"], "www.example.com")
        self.assertEqual(r["port"], 443)
        self.assertEqual(r["resource"], '/r')
        self.assertEqual(r["query"], '')

        r = utils.ws_urlparse("ws://www.example.com:8888/chat")
        self.assertEqual(r["scheme"], 'ws')
        self.assertEqual(r["host"], "www.example.com")
        self.assertEqual(r["port"], 8888)
        self.assertEqual(r["resource"], '/chat')
        self.assertEqual(r["query"], '')


        r = utils.ws_urlparse("ws://www.example.com/chat?order=1")
        self.assertEqual(r["scheme"], 'ws')
        self.assertEqual(r["host"], "www.example.com")
        self.assertEqual(r["port"], 80)
        self.assertEqual(r["resource"], '/chat')
        self.assertEqual(r["query"], 'order=1')

        r = utils.ws_urlparse("wss://[2003:4000:123:45::6]/r")
        self.assertEqual(r["scheme"], 'wss')
        self.assertEqual(r["host"], "[2003:4000:123:45::6]")
        self.assertEqual(r["port"], 443)
        self.assertEqual(r["resource"], '/r')
        self.assertEqual(r["query"], '')

    def testParseProxyUrl(self):
        r = utils.proxy_uri_parse("10.1.1.1:8080")
        self.assertEqual(r["scheme"], 'http')
        self.assertEqual(r["user"], '')
        self.assertEqual(r["passwd"], '')
        self.assertEqual(r["host"], '10.1.1.1')
        self.assertEqual(r["port"], 8080)

        r = utils.proxy_uri_parse("https://10.1.1.1:8080")
        self.assertEqual(r["scheme"], 'https')
        self.assertEqual(r["user"], '')
        self.assertEqual(r["passwd"], '')
        self.assertEqual(r["host"], '10.1.1.1')
        self.assertEqual(r["port"], 8080)

        r = utils.proxy_uri_parse("http://user:pass,.123@10.1.1.1:8080")
        self.assertEqual(r["scheme"], 'http')
        self.assertEqual(r["user"], "user")
        self.assertEqual(r["passwd"], 'pass,.123')
        self.assertEqual(r["host"], '10.1.1.1')
        self.assertEqual(r["port"], 8080)

    def testFrame(self):
        f = frame.ABNF(1,0,0,0,frame.OPCODE_CONT,0,'test')
        h = f.encode_frame()
        self.assertEqual(h, b'\x80\x04test')

        f = frame.ABNF(1,0,0,0,frame.OPCODE_TEXT,0,'test')
        h = f.encode_frame()
        self.assertEqual(h, b'\x81\x04test')

        f = frame.ABNF(1,0,0,0,frame.OPCODE_CLOSE,0,'test')
        h = f.encode_frame()
        self.assertEqual(h, b'\x88\x04test')

        f = frame.ABNF(1,0,0,0,frame.OPCODE_PING,0,'test')
        h = f.encode_frame()
        self.assertEqual(h, b'\x89\x04test')

    def testHandshakeAcceptKey(self):
        # Handshake Test vector from RFC 6455
        h = Handshake(None, "")
        h.key = "dGhlIHNhbXBsZSBub25jZQ=="
        d = h.websocket_accept_digest()
        self.assertEqual(d, b"s3pPLMBiTxaQ9kYGzzhZRbK+xOo=")

    @mock.patch('src.connect.Connect.recv', new=asyncmock(return_value=b'\x80\x09'))
    def testNormalResponse(self):
        r = frame.ResponseFrame(Connect)
        o = _run(r.recv_header())
        self.assertEqual(o, (1,0,0,0,0,0,9))

    @mock.patch('src.connect.Connect.recv', new=asyncmock(return_value=b'\xe0\x04'))
    def testWrongRsvResponse(self):
        r = frame.ResponseFrame(Connect)
        try:
            o = _run(r.recv_frame())
        except Exception as e:
            self.assertEqual(type(e), WebSocketFrameException)
        else:
            self.fail('WebSocketFrameException not raise')

    @mock.patch('src.connect.Connect.recv', new=asyncmock(return_value=b'\x87\x04'))
    def testWrongOpcodeResponse(self):
        r = frame.ResponseFrame(Connect)
        try:
            o = _run(r.recv_frame())
        except Exception as e:
            self.assertEqual(type(e), WebSocketFrameException)
        else:
            self.fail('WebSocketFrameException not raise')

    def testConnect(self):
        pass


if __name__ == '__main__':
    unittest.main()
