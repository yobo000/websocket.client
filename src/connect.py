#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Connect socket  and proxy/ssl
"""
import socket
import base64
import ssl
import os

from .exceptions import WebSocketConnectException, WebSocketTimeoutException, WebSocketConnectionClosedException
from .utils import ws_urlparse, proxy_uri_parse, _logger

__all__ = ["Connect"]


class Connect(object):

    def __init__(self, loop, options):
        self.sock = None
        self.proxy_opts = {}
        self.ssl_options = {}
        self.loop = loop
        self.options = options
        super().__init__()

    async def get_info(self, host, port):
        addr_info = await self.loop.getaddrinfo(host, port)
        if not self.options.get("ipv6", False):
            no_v6 = lambda x: True if x[2] == socket.SOL_TCP and x[0] == socket.AF_INET else False
            addr_info = list(filter(no_v6, addr_info))
        else:
            pass
        return addr_info

    async def open(self, addinfo_list):
        for addrinfo in addinfo_list:
            family, type, proto = addrinfo[:3]
            address = addrinfo[-1]

            s = socket.socket(family, type, proto)
            s.setblocking(False)
            try:
                await self.loop.sock_connect(s, address)
            except Exception as e:
                _logger.error(e)
                continue
                #
            else:
                break
        if not s:
            raise WebSocketConnectException("Build socket Error.")
        timeout = self.options.get("timeout", 10)
        s.settimeout(timeout)
        return s

    async def connect(self, url):
        url_info = ws_urlparse(url)
        proxy, is_secure = self.check_options()
        if proxy:
            self.proxy_opts = self.get_http_proxy()
            addrinfo_list = await self.get_info(self.proxy_opts["host"],
                                                self.proxy_opts["port"])
            self.sock = await self.open(addrinfo_list)
            await self.proxy_tunnel(url_info["host"], url_info["port"])
        else:
            addrinfo_list = await self.get_info(url_info["host"],
                                                url_info["port"])
            self.sock = await self.open(addrinfo_list)
        if is_secure:
            # set SSL proto version
            ssl_proto = self.options.get("ssl_proto", ssl.PROTOCOL_SSLv23)
            context = ssl.SSLContext(ssl_proto)
            # load_chain & wrap
            self.sock = self.wrap_context(context)

        if not self.sock:
            raise WebSocketConnectionClosedException("Socket has already closed.")
        return self.sock

    def check_options(self):
        proxy = True if self.options.get("enable_proxy", False) else False
        is_secure = True if self.options.get("enable_ssl", False) else False
        return proxy, is_secure

    def get_http_proxy(self):
        proxy_opts = {}
        proxy_uri = self.options.get("proxy", None)
        if not proxy_uri:
            proxy_uri = os.environ.get("http_proxy")
        proxy_opts = proxy_uri_parse(proxy_uri)
        return proxy_opts

    async def proxy_tunnel(self, host, port):
        """
        Connecting http proxy with handshake
        """
        header = [
            f"CONNECT {host}:{port} HTTP/1.1",
            f"Host: {host}",
            "Proxy-Connection: keep-alive",
            "Connection: keep-alive"
            ]

        if self.proxy_opts["user"]:
            _logger.debug("Starting connect to proxy")
            auth_string = (self.proxy_opts['user'] + ":" +
                           self.proxy_opts['passwd']).encode()
            auth_header = base64.b64encode(auth_string).strip().decode()
            header.append(f"Proxy-Authorization: Basic {auth_header}")
        proxy_header = '\r\n'.join(header) + '\r\n\r\n'
        _logger.debug("Proxy Connect Header:")
        _logger.debug(proxy_header)
        await self.send(proxy_header.encode())
        # recv exception & timeout
        line = await self.recv_line()
        line = line.decode('utf-8').strip()
        status_info = line.split(" ", 2)
        status = int(status_info[1])
        # because '\r\n' remains ``WebSocketCommonProtocol.eof_received``
        try:
            await self.recv_line()
        except TimeoutError:
            pass
        if status == 200:
            pass
            _logger.debug("Proxy Connection established.")
        else:
            raise WebSocketConnectException("Proxy Connection cannot establish.")

    def wrap_context(self, context):
        """
        :param(Optional) certfile, keyfile, ciphers, cert_chain, cert_reqs
        """
        certfile = self.options.get("certfile", None)
        if certfile and (not os.path.isfile(certfile)):
            raise FileNotFoundError("ca_cert file cannot found")
        keyfile = self.options.get("keyfile")

        # see
        # https://github.com/liris/websocket-client/commit/b96a2e8fa765753e82eea531adb19716b52ca3ca#commitcomment-10803153
        # TODO: ADD CONTEXT_CHECK_HOSTNAME

        ciphers = self.options.get('ciphers', None)
        cert_chain = self.options.get('cert_chain', None)
        ecdh_curve = self.options.get('ecdh_curve', None)
        cert_reqs = self.options.get('cert_reqs', ssl.CERT_NONE)
        context.verify_mode = cert_reqs

        if ciphers:
            context.set_ciphers(ciphers)
        if cert_chain:
            certfile, keyfile, password = cert_chain
            context.load_cert_chain(certfile, keyfile, password)
        elif certfile and keyfile:
            context.load_cert_chain(certfile, keyfile)
        if ecdh_curve:
            context.set_ecdh_curve(ecdh_curve)

        return context.wrap_socket(
            self.sock,
            do_handshake_on_connect=self.options.get('do_handshake_on_connect', True),
            suppress_ragged_eofs=self.options.get('suppress_ragged_eofs', True)
        )

    def shutdown(self):
        self.sock.shutdown(socket.SHUT_RDWR)

    def close(self):
        if not self.sock:
            raise WebSocketConnectionClosedException("Socket has already closed.")

        self.sock.close()

    async def send(self, data):
        if not self.sock:
            raise WebSocketConnectionClosedException("Socket has already closed.")

        if isinstance(data, bytes):
            return await self.loop.sock_sendall(self.sock, data)
        else:
            return "Error"

    async def recv(self, bufsize):
        try:
            bytes_string = await self.loop.sock_recv(self.sock, bufsize)
        except socket.timeout as e:
            raise TimeoutError("Receiving data timeout.")
        return bytes_string

    async def recv_line(self):
        # buffer = 1？
        line = []
        while True:
            c = await self.recv(1)
            if not c:
                break
            line.append(c)
            if c == b"\n":
                break
        return b''.join(line)
