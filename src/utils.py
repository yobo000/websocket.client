#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Parse uri
"""

from urllib.parse import urlparse
import logging
import re

from .exceptions import WebSocketAddressException

_logger = logging.getLogger('websocket')
_logger.addHandler(logging.StreamHandler())
_logger.setLevel(logging.DEBUG)


def proxy_uri_parse(proxy_uri):
    try:
        if '@' in proxy_uri:
            reg_str = '^(?P<scheme>http[s]?)?://(?P<user>[0-9a-zA-Z\.]*)?[:]?(?P<pass>[0-9a-zA-Z\.\,]*)?@(?P<host>[0-9a-zA-Z\.]*).?[:]?(?P<port>[0-9]*).?'
            m = re.search(reg_str, proxy_uri)
            result = {
                "scheme": m.group('scheme'),
                "user": m.group('user'),
                "passwd": m.group('pass'),
                "host": m.group('host'),
                "port": int(m.group('port'))
            }
        elif '://' in proxy_uri:
            reg_str = '^(?P<scheme>http[s]?)?://(?P<host>[0-9a-zA-Z\.]*).?[:]?(?P<port>[0-9]*).?'
            m = re.search(reg_str, proxy_uri)
            result = {
                "scheme": m.group('scheme'),
                "user": '',
                "passwd": '',
                "host": m.group('host'),
                "port": int(m.group('port'))
            }
        else:
            reg_str = '^(?P<host>[0-9a-zA-Z\.]*).?[:]?(?P<port>[0-9]*).?'
            m = re.search(reg_str, proxy_uri)
            result = {
                "scheme": 'http',
                "user": '',
                "passwd": '',
                "host": m.group('host'),
                "port": int(m.group('port'))
            }
    except Exception as e:
        raise WebSocketAddressException("Parsing url error ",e)

    return result


def ws_urlparse(uri):
    try:
        url_obj = urlparse(uri)
        if "::" not in url_obj.netloc and ":" in url_obj.netloc:
            host_port = url_obj.netloc.split(':')
            host = host_port[0]
            port = int(host_port[1])
        else:
            host = url_obj.netloc
            if url_obj.scheme == "ws":
                port = 80
            elif url_obj.scheme == "wss":
                port = 443
            else:
                raise WebSocketAddressException("Unknow websocket protocol.")
    except Exception as e:
        raise WebSocketAddressException("Parsing url error ", e)
    result = {
        "scheme": url_obj.scheme,
        "host": host,
        "port": port,
        "resource": url_obj.path,
        "query": url_obj.query
    }
    return result


async def check_proxy_header(conn):
    header = {}
    status = None
    status_message = ''

    _logger.debug("Receiving:")
    while True:
        line = await conn.recv_line()
        line = line.decode('utf-8').strip()
        if not line:
            break

        _logger.debug(line)
        if not status:
            status_info = line.split(" ", 2)
            status = int(status_info[1])
            status_message = status_info[2]
        else:
            kv = line.split(":", 1)
            if len(kv) == 2:
                key, value = kv
                header[key] = value.strip()
    _logger.debug("------------------")
    header["Status"] = status
    return status, header, status_message


if __name__ == "__main__":
    # result = ws_urlparse("ws://echo.websocket.org/chat")
    # print(result)
    #result = proxy_parse("http://user:pass,.123@host.name:8000")
    result = proxy_uri_parse("http://host.name:8000")
    print(result)
