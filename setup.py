import setuptools

setuptools.setup(
    name='websocket.client',
    packages=['src'],
    description="An implementation of the WebSocket Protocol Client (RFC 6455)",
    url='https://gitlab.com/yobo000/websocket.client',
    license='BSD',
    ext_modules=[
        setuptools.Extension(
            'websocket.client.speedups',
            sources=['src/speedups.c']
        )
    ]
)
