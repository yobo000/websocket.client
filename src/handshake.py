#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
websocket Handshake: https://tools.ietf.org/html/rfc6455#page-14
"""
from secrets import token_bytes
from hashlib import sha1
import base64

from .exceptions import WebSocketHandshakeException
from .utils import ws_urlparse, _logger

__all__ = ["Handshake"]

VERSION = 13


def accept_key():
    pass


def create_key():
    key = token_bytes(16)
    return base64.b64encode(key).decode('utf-8')


class Handshake(object):

    def __init__(self, conn, url):
        self.conn = conn
        self._url = url

    def urlparse(self):
        url_info = ws_urlparse(self._url)
        self._host = url_info["host"]
        self._port = url_info["port"]
        self._resource = url_info["resource"]
        self._protocols = url_info["resource"][1:]

    def get_header(self):
        self.urlparse()
        header = [
            f"GET {self._resource} HTTP/1.1",
            f"Host: {self._host}",
            f"Origin: http://{self._host}",
            "Upgrade: websocket",
            "Connection: Upgrade"
        ]
        # Origin
        self.key = create_key()
        header.append(f"Sec-WebSocket-Key: {self.key}")
        header.append(f"Sec-WebSocket-Version: {VERSION}")
        if self._protocols:
            sub_protocol = ','.join(self._protocols)
            header.append(f"Sec-WebSocket-Protocol: {sub_protocol}")
        return header

    async def response_header(self):
        #
        header = {}
        status = None
        status_message = ''

        _logger.debug("Receiving:")
        while True:
            line = await self.conn.recv_line()
            line = line.decode('utf-8').strip()
            if not line:
                break

            _logger.debug(line)
            if not status:
                status_info = line.split(" ", 2)
                status = int(status_info[1])
                status_message = status_info[2]
            else:
                kv = line.split(":", 1)
                if len(kv) == 2:
                    key, value = kv
                    header[key] = value.strip()
        _logger.debug("------------------")
        header["Status"] = status
        self.check_header(header)
        return status, header, status_message

    def check_header(self, header):
        if not header:
            raise WebSocketHandshakeException("Empty Handshake response")
        try:

            assert header['Upgrade'].lower() == "websocket"
            assert header['Connection'].lower() == "upgrade"
            assert header["Status"] == 101
            digest = self.websocket_accept_digest()
            assert digest == header['Sec-WebSocket-Accept'].encode()
        except Exception as e:
            raise WebSocketHandshakeException(f"Handshake response error with Header: {e}.")
        else:
            return True

    def websocket_accept_digest(self):
        # 神秘代码: 258EAFA5-E914-47DA-95CA-C5AB0DC85B11
        magic_string = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
        value = (self.key + magic_string).encode('utf-8')
        digest = base64.b64encode(sha1(value).digest())
        return digest

    async def send_header(self):
        header = self.get_header()
        header_str = '\r\n'.join(header)
        _logger.debug(f"Sending: \n{header_str}")
        _logger.debug("------------------------")
        header_str += '\r\n\r\n'
        await self.conn.send(header_str.encode("utf-8"))
        return await self.response_header()
