#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
import asyncio

from .handshake import Handshake
from .connect import Connect
from .frame import *


class Client(object):
    """
    options:
    `enable_proxy`:
    `proxy`:
    `enable_ssl`:
    ``ssl_options``include: certfile, keyfile, ciphers, cert_chain, cert_reqs
    `ipv6`:
    `timeout`:
    """
    def __init__(self, url="", **options):
        self._url = url
        self._sock = None
        self._conn = None
        self.options = options

    async def open(self):
        loop = asyncio.get_event_loop()
        self._conn = Connect(loop, self.options)

        if not self._url:
            raise ValueError("URI is empty.")

        self._sock = await self._conn.connect(self._url)
        hs = Handshake(self._conn, self._url)
        status, headers, status_message = await hs.send_header()
        return status, headers, status_message

    async def _send(self, frame):
        await self._conn.send(frame.encode_frame())

    async def send(self, data):
        # default OPCODE_TEXT
        frame = ABNF.create(OPCODE_TEXT, data)
        await self.send_frame(frame)

    async def send_frame(self, frame):
        await self._send(frame)

    async def send_binary(self, data):
        # OPCODE_BINARY
        frame = ABNF.create(OPCODE_BINARY, data)
        await self._send(frame)

    async def send_continue(self, data):
        # OPCODE_CONT
        frames = ABNF.create(OPCODE_TEXT, data)
        for frame in frames.encode_frame(cont=True):
            await self._conn.send(self._sock, frame)

    async def ping(self, data):
        # OPCODE_PING
        frame = ABNF.create(OPCODE_PING, data)
        await self._send(frame)

    async def pong(self, data):
        # OPCODE_PONG
        frame = ABNF.create(OPCODE_PONG, data)
        await self._send(frame)

    async def send_close(self, status=STATUS_NORMAL, reason="timeout"):
        # OPCODE_CLOSE
        reason = reason.encode('latin-1')
        data = struct.pack('!H', status) + reason
        frame = ABNF.create(OPCODE_CLOSE, data)
        await self._send(frame)

    async def exit(self):
        # await self.send_close()
        self._conn.close()
        print("closing...")

    async def recv(self, loop=True):
        self.settimeout(10)
        frame = ResponseFrame(self._conn)
        if not frame:
            raise Exception
        data, opcode = await frame.recv_frame()
        if opcode in [OPCODE_TEXT, OPCODE_BINARY]:
            return data
        elif opcode == OPCODE_PING:
            if len(data)>126:
                self.pong(data)
                return { "PING": data }
            else:
                print("too long ping")
        elif opcode == OPCODE_PONG:
            # if active check_pong
            return { "PONG": data }
        elif opcode == OPCODE_CLOSE:
            ResponseFrame.check_data(data)
            self.exit()
            return "CLOSE"

    def settimeout(self, timeout):
        if self._sock:
            self._sock.settimeout(timeout)

    def fileno(self):
        if self._sock:
            return self._sock.fileno()

    async def __aenter__(self):
        await self.open()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.exit()

    def __aiter__(self):
        return self

    async def __anext__(self):
        try:
            data = await self.recv()
            if data:
                return data
            else:
                raise StopAsyncIteration
        except TimeoutError as e:
            await self.exit()
