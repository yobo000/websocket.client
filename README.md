#### websocket-client
![](https://img.shields.io/badge/python-3.6-blue.svg)

An implement of websocket client.
Reference: [https://tools.ietf.org/html/rfc6455](https://tools.ietf.org/html/rfc6455)

Features:
- Supports HTTP Proxies.
- Supports IPv4/IPv6.
- Supports SSL certificate pinning.
- Sends ping and can process pong events.
- Asynchronous and non-blocking.

Installation:
```sh
git clone https://gitlab.com/yobo000/websocket.client.git
cd webscoket.client
python setup.py
```

Usage:
Simple(like `socket`):
```python
import asyncio
from websocket.client import Client

async def hello(url, **kw):
    client = Client(url, **kw)
    await client.open()
    await client.send("Hi, websocket.")
    result = await client.recv()
    print(result)
    await client.exit()

asyncio.get_event_loop().run_until_complete(
    hello("ws://echo.websocket.org/",
          enable_proxy=True,
          proxy = "http://http.proxy:8888"))
```

Iterator with Context manager:
```python
import asyncio
from websocket.client import Client

async with Client(url, **kw) as ws:
    await ws.send("Hi, WebSocket!")
    async for msg in ws:
       print(msg)

asyncio.get_event_loop().run_until_complete(
    hello("ws://echo.websocket.org/")
```

Proxy:
```
Client(url, enable_proxy=True, proxy = "http://localhost:8888")
```

SSL:
```
# SSL options: certfile, keyfile, ciphers, cert_chain, cert_reqs
Client(url, enable_ssl=True, certfile=xxx, keyfile=xxx)
```
