#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__all__ = [
    'WebSocketException', 'WebSocketConnectException', 'WebSocketHandshakeException',
    'WebSocketFrameException', 'WebSocketConnectionClosedException', 'WebSocketTimeoutException',
    'WebSocketProxyException', 'WebSocketBadStatusException', 'WebSocketAddressException'
]

class WebSocketException(Exception):
    """
    websocket exception class.
    """
    pass


class WebSocketConnectException(WebSocketException):
    """
    If the websocket protocol is invalid, this exception will be raised.
    """
    pass


class WebSocketHandshakeException(WebSocketException):
    """
    If the websocket handshake is invalid, this exception will be raised.
    """
    pass


class WebSocketFrameException(WebSocketException):
    """
    If the websocket frame bit is invalid, this exception will be raised.
    """
    pass


class WebSocketConnectionClosedException(WebSocketException):
    """
    If remote host closed the connection or some network error happened,
    this exception will be raised.
    """
    pass


class WebSocketTimeoutException(WebSocketException):
    """
    WebSocketTimeoutException will be raised at socket timeout during read/write data.
    """
    pass


class WebSocketProxyException(WebSocketException):
    """
    WebSocketProxyException will be raised when proxy error occurred.
    """
    pass


class WebSocketBadStatusException(WebSocketException):
    """
    WebSocketBadStatusException will be raised when we get bad handshake status code.
    """
    pass


class WebSocketAddressException(WebSocketException):
    """
    If the websocket address info cannot be found, this exception will be raised.
    """
    pass
